# Rover Client Coding Project

The goal of this project to create a client app (web, iOS, Android, React Native, etc) to store a database of dogs and their owners. Note: this should use client-side persistence (i.e. local storge, Core Data, Room, etc). You do not need to implement a server to store this data.

You can use any tools or frameworks you’d like to complete this project.

While the project itself is simple, we're not just looking to see if you can complete the project.

The work you create here should be representative of code that we'd expect to receive from you if you were hired tomorrow. Our expectation is that you'll write production quality code including tests.

Update the existing README to help us understand your approach and thought process, e.g. design choices, trade-offs, dependencies, etc.

Finally, this is not a trick project, so if you have any questions, don't hesitate to ask.

### Requirements

1. A user should be able to create a dog by entering:

- The dog's name

- A description of the dog

- A photo of the dog

2. The newly created dog should display in a list of all dogs with their description.

3. Each dog in the list should show their name, a thumbnail photo and the description.

4. Use a persistent store (i.e. local storage, Core Data, Realm, etc)

5. Minimum API requirements (for mobile):

- iOS: Minimum API target should be iOS 11+

- Android: Minimum API target should be 19

We would like to see some tests around business critical areas so we have an idea of how you write tests.

### Design

At Rover, we use a tool called Figma to collaborate on new features and designs. You can access the designs and specs for this project [here](https://www.figma.com/file/e43IBPwTuO7u5iMjVpKQVx/Rover-Coding-Project?node-id=279%3A1519).

Complete this project and push your solution back to this repository.

When you're finished, please email your Rover contact and let them know.

Be aware that we will have you use and modify this project for the in-person portion of the interview.

### Candidate Comments

Application has been made with [Create React App](https://create-react-app.dev/) using Typescript. Key third-party libraries:

- [Zustand](https://zustand-demo.pmnd.rs/): State manager. Simpler boilerplate than Redux, and more efficient than React Context API. Also makes easy to persist state on LocalStorage
- [Mantine](https://mantine.dev/): UI Kit. Similar to Material UI, but less opinionated design
