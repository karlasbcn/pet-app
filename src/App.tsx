import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import { Home, Add, Edit } from 'pages'
import { StyleProvider } from 'providers'
import { AppShell } from 'components'

const routes = [
  {
    path: '/',
    element: <Home />,
  },
  {
    path: '/add',
    element: <Add />,
  },
  {
    path: '/edit/:petId',
    element: <Edit />,
  },
]

const router = createBrowserRouter(
  routes.map(({ path, element }) => {
    return { path, element: <AppShell>{element}</AppShell> }
  })
)

export default function App() {
  return (
    <StyleProvider>
      <RouterProvider router={router} />
    </StyleProvider>
  )
}
