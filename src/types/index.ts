export type Pet = {
  id: number
  name: string
  description?: string
  image?: string
}

export type NewPet = Omit<Pet, 'id'>
