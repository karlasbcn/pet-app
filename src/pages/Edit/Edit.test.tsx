import { fireEvent, render, screen } from '@testing-library/react'
import { useParams } from 'react-router-dom'
import { usePetStore } from 'hooks'
import Edit from './Edit'
import type { Pet } from 'types'

jest.mock('hooks/')
const mockUsePetStore = usePetStore as jest.MockedFunction<typeof usePetStore>
const mockPet: Pet = { id: 1, name: 'Garfield', description: 'Cat' }
const mockEditPet = jest.fn()
const mockRemovePet = jest.fn()
const mockUsePetStoreReturnValue = {
  pets: [mockPet],
  addPet: () => null,
  editPet: mockEditPet,
  removePet: mockRemovePet,
  username: 'Carles',
}

const mockUseParams = useParams as jest.Mock

describe('Edit Page', () => {
  test('renders with an existing pet', () => {
    mockUseParams.mockReturnValue({ petId: mockPet.id.toString() })
    mockUsePetStore.mockReturnValue(mockUsePetStoreReturnValue)
    render(<Edit />)
    const nameElement = screen.getByLabelText(/Name/i)
    const descriptionElement = screen.getByLabelText(/About your pet/i)
    const submitElement = screen.getByText(/Save pet/i)
    const removeElement = screen.getByText(/Remove pet/i)
    expect(nameElement).toBeInTheDocument()
    expect(nameElement).toHaveDisplayValue(mockPet.name)
    expect(descriptionElement).toBeInTheDocument()
    expect(descriptionElement).toHaveDisplayValue(mockPet.description!)
    expect(submitElement).toBeInTheDocument()
    expect(removeElement).toBeInTheDocument()
    expect(mockEditPet).not.toHaveBeenCalled()
    fireEvent.click(submitElement)
    expect(mockEditPet).toHaveBeenCalled()
    expect(mockRemovePet).not.toHaveBeenCalled()
    fireEvent.click(removeElement)
    expect(mockRemovePet).toHaveBeenCalled()
  })

  test('redirects when pet with given id does not exist', () => {
    mockUseParams.mockReturnValue({ petId: '123456789' })
    mockUsePetStore.mockReturnValue(mockUsePetStoreReturnValue)
    render(<Edit />)
    const mockNavigateElement = screen.getByText(/Navigate component/i)
    expect(mockNavigateElement).toBeInTheDocument()
  })
})
