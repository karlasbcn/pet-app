import { useNavigate, useParams, Navigate } from 'react-router-dom'
import { PetForm } from 'components'
import { usePetStore } from 'hooks'
import type { NewPet } from 'types'

export default function Edit() {
  const { editPet, removePet, pets } = usePetStore()
  const navigate = useNavigate()
  const params = useParams()
  const petId = Number(params.petId)
  const pet = pets.find(({ id }) => id === petId)

  if (!pet) {
    return <Navigate to="/" replace />
  }

  function handleSubmit(updatedPet: NewPet) {
    editPet(petId, updatedPet)
    navigate('/')
  }

  function handleRemove() {
    removePet(petId)
    navigate('/')
  }

  return (
    <PetForm onSubmit={handleSubmit} onRemove={handleRemove} editingPet={pet} />
  )
}
