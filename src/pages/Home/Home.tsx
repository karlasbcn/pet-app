import { AddCTA, PetEntry, Title, Account } from 'components'
import { usePetStore } from 'hooks'

export default function Home() {
  const { pets, username } = usePetStore()
  const empty = pets.length === 0
  return (
    <>
      {empty ? <Account name={username} /> : <Title text="Your pets" />}
      {pets.map((pet) => (
        <PetEntry key={pet.id} pet={pet} navigateTo={`/edit/${pet.id}`} />
      ))}
      <AddCTA
        small={!empty}
        text={empty ? 'Add pet' : 'Add another pet'}
        navigateTo="/add"
      />
    </>
  )
}
