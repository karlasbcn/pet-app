import { render, screen } from '@testing-library/react'
import { BrowserRouter } from 'react-router-dom'
import { usePetStore } from 'hooks'
import Home from './Home'

jest.mock('hooks/')
const mockUsePetStore = usePetStore as jest.MockedFunction<typeof usePetStore>

describe('Home page', () => {
  test('renders with pets', () => {
    mockUsePetStore.mockReturnValue({
      pets: [
        { id: 1, name: 'Garfield' },
        { id: 2, name: 'Odie', description: 'Dog' },
      ],
      addPet: () => null,
      editPet: () => null,
      removePet: () => null,
      username: 'Carles',
    })
    render(<Home />, { wrapper: BrowserRouter })
    const ctaElement = screen.getByText(/Add another pet/i)
    const garfieldElement = screen.getByText(/Garfield/i)
    const odieNameElement = screen.getByText(/Odie/i)
    const odieDescriptionElement = screen.getByText(/Dog/i)
    const titleElement = screen.getByText(/Your pets/i)
    const accountElement = screen.queryByText(/Carles/i)
    const petEntryElements = screen.getAllByRole('pet-entry')
    expect(ctaElement).toBeInTheDocument()
    expect(garfieldElement).toBeInTheDocument()
    expect(odieNameElement).toBeInTheDocument()
    expect(odieDescriptionElement).toBeInTheDocument()
    expect(titleElement).toBeInTheDocument()
    expect(petEntryElements).toHaveLength(2)
    expect(accountElement).not.toBeInTheDocument()
  })

  test('renders with no pets', () => {
    mockUsePetStore.mockReturnValue({
      pets: [],
      addPet: () => null,
      editPet: () => null,
      removePet: () => null,
      username: 'Carles',
    })
    render(<Home />, { wrapper: BrowserRouter })
    const ctaElement = screen.getByText(/Add pet/i)
    const titleElement = screen.queryByText(/Your pets/i)
    const accountElement = screen.getByText(/Carles/i)
    const petEntryElement = screen.queryByRole('pet-entry')
    expect(ctaElement).toBeInTheDocument()
    expect(titleElement).not.toBeInTheDocument()
    expect(petEntryElement).not.toBeInTheDocument()
    expect(accountElement).toBeInTheDocument()
  })
})
