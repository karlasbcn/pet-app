import { useNavigate } from 'react-router-dom'
import { PetForm } from 'components'
import { usePetStore } from 'hooks'
import type { NewPet } from 'types'

export default function Add() {
  const { addPet } = usePetStore()
  const navigate = useNavigate()

  function handleSubmit(newPet: NewPet) {
    addPet(newPet)
    navigate('/')
  }

  return <PetForm onSubmit={handleSubmit} />
}
