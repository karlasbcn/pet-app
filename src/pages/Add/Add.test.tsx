import { render, screen, fireEvent } from '@testing-library/react'
import Add from './Add'
import { usePetStore } from 'hooks'

jest.mock('hooks/')
const mockUsePetStore = usePetStore as jest.MockedFunction<typeof usePetStore>

describe('AddPage', () => {
  test('renders', () => {
    const mockAddPet = jest.fn()
    mockUsePetStore.mockReturnValue({
      pets: [],
      addPet: mockAddPet,
      editPet: () => null,
      removePet: () => null,
      username: 'Carles',
    })
    render(<Add />)
    const nameElement = screen.getByLabelText(/Name/i)
    const descriptionElement = screen.getByLabelText(/About your pet/i)
    const submitElement = screen.getByText(/Save pet/i)
    const removeElement = screen.queryByText(/Remove pet/i)
    expect(nameElement).toBeInTheDocument()
    expect(nameElement).toHaveDisplayValue('')
    expect(descriptionElement).toBeInTheDocument()
    expect(descriptionElement).toHaveDisplayValue('')
    expect(submitElement).toBeInTheDocument()
    expect(removeElement).not.toBeInTheDocument()
    expect(mockAddPet).not.toHaveBeenCalled()
    fireEvent.click(submitElement)
    expect(mockAddPet).not.toHaveBeenCalled()
    fireEvent.change(screen.getByLabelText(/Name/i), {
      target: { value: 'Garfield' },
    })
    fireEvent.click(submitElement)
    expect(mockAddPet).toHaveBeenCalled()
  })
})
