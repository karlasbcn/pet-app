import create from 'zustand'
import { persist } from 'zustand/middleware'
import type { Pet, NewPet } from 'types'

const USERNAME = 'Amy S.'

function buildPet(
  id: Pet['id'],
  name: Pet['name'],
  description: Pet['description'],
  image: Pet['image']
) {
  const pet: Pet = { id, name }
  if (description) {
    pet.description = description
  }
  if (image) {
    pet.image = image
  }
  return pet
}

interface PetStore {
  pets: Pet[]
  addPet: (pet: NewPet) => void
  editPet: (petId: Pet['id'], values: NewPet) => void
  removePet: (petId: Pet['id']) => void
}

const useStore = create<PetStore>()(
  persist(
    (set, get) => ({
      pets: [],
      addPet: ({ name, description, image }) => {
        const { pets } = get()
        const id = 1 + (pets.length ? Math.max(...pets.map(({ id }) => id)) : 0)
        const newPet = buildPet(id, name, description, image)
        set({ pets: [newPet, ...pets] })
      },
      editPet: (id, { name, description, image }) => {
        const updatedPets = get().pets.map((pet) => {
          if (pet.id !== id) {
            return pet
          }
          return buildPet(id, name, description, image)
        })
        set({ pets: updatedPets })
      },
      removePet: (id) => {
        const updatedPets = get().pets.filter((pet) => pet.id !== id)
        set({ pets: updatedPets })
      },
    }),
    { name: 'petStore' }
  )
)

interface ReturnValue extends PetStore {
  username: string
}

export default function usePetStore(): ReturnValue {
  return { ...useStore(), username: USERNAME }
}
