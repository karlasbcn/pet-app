import { renderHook, act } from '@testing-library/react-hooks'
import usePetStore from './usePetStore'

describe('usePetStore', () => {
  test('adds new pet', () => {
    const { result } = renderHook(usePetStore)
    expect(result.current.pets.length).toEqual(0)
    act(() => result.current.addPet({ name: 'Garfield' }))
    expect(result.current.pets.length).toEqual(1)
    const garfield = result.current.pets.find(
      ({ name }) => name === 'Garfield'
    )!
    expect(garfield.id).toEqual(1)
    expect(Object.keys(garfield).includes('description')).toBe(false)
    expect(Object.keys(garfield).includes('image')).toBe(false)
    act(() =>
      result.current.addPet({ name: 'Odie', description: 'Dog', image: '' })
    )
    expect(result.current.pets.length).toEqual(2)
    const odie = result.current.pets.find(({ name }) => name === 'Odie')!
    expect(odie.id).toEqual(2)
    expect(Object.keys(odie).includes('description')).toBe(true)
    expect(Object.keys(odie).includes('image')).toBe(false)
  })

  test('edits a pet', () => {
    const { result } = renderHook(usePetStore)
    expect(result.current.pets.length).toEqual(2)
    act(() => result.current.editPet(1, { name: 'Jinks' }))
    const petNames = result.current.pets.map(({ name }) => name)
    expect(petNames.includes('Odie')).toBe(true)
    expect(petNames.includes('Garfield')).toBe(false)
    expect(petNames.includes('Jinks')).toBe(true)
  })

  test('removes a pet', () => {
    const { result } = renderHook(usePetStore)
    expect(result.current.pets.length).toEqual(2)
    act(() => result.current.removePet(1))
    expect(result.current.pets.length).toEqual(1)
    expect(result.current.pets[0].name).toEqual('Odie')
  })
})
