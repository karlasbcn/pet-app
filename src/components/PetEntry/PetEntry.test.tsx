import { render, screen } from '@testing-library/react'
import { BrowserRouter } from 'react-router-dom'
import PetEntry from './PetEntry'
import type { Pet } from 'types'

describe('PetEntry', () => {
  test('renders without image', () => {
    const garfield: Pet = { id: 1, name: 'Garfield' }
    render(<PetEntry navigateTo="/" pet={garfield} />, {
      wrapper: BrowserRouter,
    })
    expect(screen.getByText('Garfield')).toBeInTheDocument()
    expect(screen.queryByRole('img')).not.toBeInTheDocument()
  })
  test('renders with image', () => {
    const odie: Pet = {
      id: 2,
      name: 'Odie',
      description: 'Dog',
      image: 'data://abcde',
    }
    render(<PetEntry navigateTo="/" pet={odie} />, {
      wrapper: BrowserRouter,
    })
    expect(screen.getByText('Odie')).toBeInTheDocument()
    expect(screen.getByText('Dog')).toBeInTheDocument()
    expect(screen.getByRole('img')).toBeInTheDocument()
  })
})
