import type { MantineTheme } from '@mantine/core'

export default function boxStyle(theme: MantineTheme) {
  return {
    width: '100%',
    padding: `${theme.spacing.md}px 0`,
    boxShadow: `inset 0 -1px 0 ${theme.colors.gray[3]}`,
    '& .mantine-Avatar-root': {
      marginRight: theme.spacing.md,
    },
    '& svg': {
      transform: 'scale(1.15)',
    },
    '& a': {
      marginLeft: theme.spacing.md,
      textDecoration: 'none',
      '& > div': {
        color: `${theme.colors.blue[5]} !important`,
      },
      '&:hover': {
        textDecoration: 'underline',
      },
    },
    '& .desc': {
      color: theme.colors.gray[7],
    },
  }
}
