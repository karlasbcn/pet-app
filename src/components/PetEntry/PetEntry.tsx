import { Avatar, Flex, Text, Box } from '@mantine/core'
import { Link } from 'react-router-dom'
import { EmptyAvatar } from 'components/icons'
import boxStyle from './PetEntry.styles'
import type { Pet } from 'types'

interface PetEntryProps {
  pet: Pet
  navigateTo: string
}

export default function PetEntry({ pet, navigateTo }: PetEntryProps) {
  const { name, description, image } = pet
  return (
    <Box sx={boxStyle} role="pet-entry">
      <Flex justify="space-between" align="center">
        <Flex align="center">
          <Avatar radius="xl" size="lg" src={image} color="gray.3">
            {image ? null : <EmptyAvatar />}
          </Avatar>
          <Flex direction="column">
            <Text fz={14} weight="bold">
              {name}
            </Text>
            {description ? (
              <Text className="desc" lineClamp={1}>
                {description}
              </Text>
            ) : null}
          </Flex>
        </Flex>
        <Link to={navigateTo}>
          <Text>Edit</Text>
        </Link>
      </Flex>
    </Box>
  )
}
