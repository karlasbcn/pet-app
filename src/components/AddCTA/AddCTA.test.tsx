import { render, screen } from '@testing-library/react'
import { BrowserRouter } from 'react-router-dom'
import AddCTA from './AddCTA'

describe('AddCTA', () => {
  test('renders big', () => {
    render(<AddCTA navigateTo="/" text="Big" />, {
      wrapper: BrowserRouter,
    })
    expect(screen.getByText('Big')).toBeInTheDocument()
    expect(screen.getByRole('addCTA-big')).toBeInTheDocument()
  })
  test('renders small', () => {
    render(<AddCTA navigateTo="/" text="Small" small />, {
      wrapper: BrowserRouter,
    })
    expect(screen.getByText('Small')).toBeInTheDocument()
    expect(screen.getByRole('addCTA-small')).toBeInTheDocument()
  })
})
