import { Text, Box, Flex } from '@mantine/core'
import { Link } from 'react-router-dom'
import { Plus, PlusCircle } from 'components/icons'
import boxStyle from './AddCTA.styles'

interface AddCTAProps {
  text: string
  navigateTo: string
  small?: boolean
}

export default function AddCTA({
  text,
  navigateTo,
  small = false,
}: AddCTAProps) {
  return (
    <Box sx={boxStyle(small)} role={`addCTA-${small ? 'small' : 'big'}`}>
      <Link to={navigateTo}>
        <Flex
          direction={small ? 'row' : 'column'}
          align="center"
          justify="center"
        >
          {small ? <PlusCircle /> : <Plus />}
          <Text fz={14}>{text}</Text>
        </Flex>
      </Link>
    </Box>
  )
}
