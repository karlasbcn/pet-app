import type { MantineTheme } from '@mantine/core'

export default function boxStyle(small: boolean) {
  return function (theme: MantineTheme) {
    return {
      background: small ? 'transparent' : theme.colors.gray[0],
      height: small ? 'auto' : 188,
      width: small ? 'fit-content' : '100%',
      border: small ? 0 : `2px dashed ${theme.colors.gray[1]}`,
      padding: `${theme.spacing.md}px 0`,
      '& a': {
        textDecoration: 'none',
        lineHeight: small ? 'inherit' : 2.5,
        '& > div': {
          height: '100%',
          '& > div': {
            color: `${
              small ? theme.colors.blue[5] : theme.colors.gray[9]
            } !important`,
          },
        },
      },
      '&:hover a': {
        textDecoration: 'underline',
      },
      '& svg': {
        marginRight: small ? theme.spacing.xs : 0,
        marginBottom: small ? 0 : 10,
        '& path': {
          fill: small ? theme.colors.blue[5] : theme.colors.gray[9],
        },
      },
    }
  }
}
