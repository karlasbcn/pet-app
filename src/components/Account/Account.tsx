import { Flex, Text } from '@mantine/core'
import { Link } from 'react-router-dom'
import { User, EmptyAvatar } from 'components/icons'
import boxStyle from './Account.styles'

export default function Account({ name }: { name: string }) {
  return (
    <Flex direction="column" sx={boxStyle} mb="lg" pt="md">
      <Flex align="center" pb="md">
        <User />
        <Text ml="sm" fz={16} fw={600}>
          AccountInfo
        </Text>
      </Flex>
      <Flex pt="md">
        <EmptyAvatar />
        <Flex direction="column" ml="sm">
          <Text fw={600} mb="sm">
            {name}
          </Text>
          <Link to="/">Edit</Link>
        </Flex>
      </Flex>
    </Flex>
  )
}
