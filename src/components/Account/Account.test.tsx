import { render, screen } from '@testing-library/react'
import { BrowserRouter } from 'react-router-dom'
import Account from './Account'

describe('Account', () => {
  test('renders', () => {
    const name = 'Carles'
    render(<Account name={name} />, {
      wrapper: BrowserRouter,
    })
    expect(screen.getByText(name)).toBeInTheDocument()
  })
})
