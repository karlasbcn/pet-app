import type { MantineTheme } from '@mantine/core'

export default function boxStyle(theme: MantineTheme) {
  return {
    boxShadow: '0 4px 16px rgba(0, 0, 0, 0.12)',
    padding: theme.spacing.md,
    background: 'white',
    '& > div:first-of-type': {
      borderBottom: `1px solid ${theme.colors.gray[0]}`,
    },
    '& a': {
      color: theme.colors.blue[5],
      textDecoration: 'none',
      '&:hover': {
        textDecoration: 'underline',
      },
    },
  }
}
