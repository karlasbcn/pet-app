import {
  AppShell as MantineAppShell,
  Header as MantineHeader,
  Container,
  Flex,
  MediaQuery,
  Text,
} from '@mantine/core'
import { Link } from 'react-router-dom'
import { usePetStore } from 'hooks'
import { Logo, Bell, Menu, Search, EmptyAvatarSmall } from 'components/icons'
import { headerStyle, appShellStyle } from './AppShell.styles'

function Header() {
  const { username } = usePetStore()
  return (
    <MantineHeader height={50} sx={headerStyle}>
      <Flex>
        <Link to="/" className="logo">
          <Logo />
        </Link>
        <Flex className="itemHeader" align="center">
          <Search />
          <Text>Search Sitters</Text>
        </Flex>
      </Flex>
      <Flex align="center">
        <Flex className="itemHeader" align="center">
          <EmptyAvatarSmall />
          <Text>{username}</Text>
        </Flex>
        <Flex mr="sm" align="center" justify="center">
          <Bell />
        </Flex>
        <MediaQuery largerThan="sm" styles={{ display: 'none' }}>
          <Menu />
        </MediaQuery>
      </Flex>
    </MantineHeader>
  )
}

interface AppShellProps {
  children: JSX.Element
}

export default function AppShell({ children }: AppShellProps) {
  return (
    <MantineAppShell sx={appShellStyle} padding="md" header={<Header />}>
      <Container size="xs">{children}</Container>
    </MantineAppShell>
  )
}
