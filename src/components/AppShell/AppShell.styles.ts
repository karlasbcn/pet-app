import type { MantineTheme } from '@mantine/core'

export function appShellStyle(theme: MantineTheme) {
  return {
    '& .mantine-AppShell-main': {
      [theme.fn.smallerThan('sm')]: {
        background: theme.colors.gray[0],
      },
    },
  }
}

export function headerStyle(theme: MantineTheme) {
  return {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: `${theme.spacing.xs}px ${theme.spacing.xl}px`,
    boxShadow: `0 1px 0 ${theme.colors.gray[1]}`,
    border: 0,
    '& .logo svg': {
      marginTop: 6,
    },
    '& .itemHeader': {
      margin: `0 ${theme.spacing.lg}px`,
      '& svg': {
        marginRight: theme.spacing.xs,
        height: 14,
        width: 14,
      },
      [theme.fn.smallerThan('sm')]: {
        display: 'none',
      },
    },
  }
}
