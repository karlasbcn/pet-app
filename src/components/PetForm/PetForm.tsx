import {
  TextInput,
  Textarea,
  Button,
  Footer,
  Container,
  Box,
  Text,
} from '@mantine/core'
import { useForm } from '@mantine/form'
import omit from 'lodash.omit'
import { Title, ImagePicker } from 'components'
import { Paw } from 'components/icons'
import boxStyle from './PetForm.styles'
import type { Pet, NewPet } from 'types'

interface PetFormProps {
  onSubmit: (values: NewPet) => void
  onRemove?: () => void
  editingPet?: Pet
}

export default function PetForm({
  onSubmit,
  onRemove,
  editingPet,
}: PetFormProps) {
  const form = useForm<NewPet>({
    initialValues: editingPet
      ? omit(editingPet, 'id')
      : {
          name: '',
          description: '',
          image: '',
        },
    validate: {
      name: (name) => (name.length ? null : 'Insert a name for your pet'),
    },
  })

  return (
    <Box sx={boxStyle}>
      <form onSubmit={form.onSubmit(onSubmit)}>
        <Title text="Tell us about your pet" />
        <ImagePicker
          label="Upload Pet Photo"
          {...form.getInputProps('image')}
        />
        <Title text="Pet details" icon={<Paw />} />
        <Text className="sub" fz={16}>
          Provide a description of your pet
        </Text>
        <Box className="inputGroup">
          <TextInput label="Pet Name" {...form.getInputProps('name')} />
          <Textarea
            label="About your pet"
            placeholder="Add a description of your pet"
            {...form.getInputProps('description')}
          />
        </Box>
        <Footer fixed height={80}>
          <Container size="xs">
            {onRemove ? (
              <Button onClick={onRemove} variant="white" color="gray">
                Remove pet
              </Button>
            ) : (
              <span />
            )}
            <Button radius="xl" type="submit">
              Save pet
            </Button>
          </Container>
        </Footer>
      </form>
    </Box>
  )
}
