import type { MantineTheme } from '@mantine/core'

export default function boxStyle(theme: MantineTheme) {
  return {
    '& .mantine-Footer-root': {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      padding: theme.spacing.xs,
      border: 0,
      boxShadow: '0px 4px 32px rgba(0, 0, 0, 0.14)',
      '& > div': {
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between',
      },
      '& .mantine-Button-label': {
        fontSize: 16,
      },
    },
    '& .mantine-InputWrapper-root': {
      marginTop: theme.spacing.xl,
      '& .mantine-InputWrapper-label': {
        fontSize: 16,
        fontWeight: 700,
        marginBottom: theme.spacing.sm,
      },
      '& .mantine-Input-input': {
        background: 'white',
        fontSize: theme.fontSizes.md,
        '&.mantine-Textarea-input': {
          height: 168,
          '&::placeholder': {
            color: theme.colors.gray[7],
          },
        },
      },
    },
    '& .mantine-InputWrapper-error': {
      fontSize: theme.fontSizes.md,
    },
    '& .sub': {
      color: theme.colors.gray['7'],
    },
    '& .inputGroup': {
      [theme.fn.smallerThan('sm')]: {
        background: 'white',
        boxShadow: '0px 4px 16px rgba(0, 0, 0, 0.12)',
        borderRadius: theme.spacing.xs,
        padding: theme.spacing.md,
        paddingBottom: theme.spacing.lg,
        marginTop: theme.spacing.sm,
      },
    },
  }
}
