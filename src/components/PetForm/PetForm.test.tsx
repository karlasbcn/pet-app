import { render, screen, fireEvent } from '@testing-library/react'
import PetForm from './PetForm'

describe('PetForm', () => {
  const onSubmit = jest.fn()
  test('renders', () => {
    render(<PetForm onSubmit={onSubmit} />)
    const nameElement = screen.getByText(/Name/i)
    const descriptionElement = screen.getByText(/Description/i)
    const submitElement = screen.getByText(/Save pet/i)
    const removeElement = screen.queryByText(/Remove pet/i)
    const titleElement = screen.queryByText(/Tell us about your pet/i)
    const imagePickerElement = screen.queryByText(/Upload Pet Photo/i)
    expect(nameElement).toBeInTheDocument()
    expect(descriptionElement).toBeInTheDocument()
    expect(submitElement).toBeInTheDocument()
    expect(removeElement).not.toBeInTheDocument()
    expect(titleElement).toBeInTheDocument()
    expect(imagePickerElement).toBeInTheDocument()
  })
  test('name is mandatory', () => {
    const name = 'Garfield'
    render(<PetForm onSubmit={onSubmit} />)
    const submitElement = screen.getByText(/Save pet/i)
    expect(
      screen.queryByText(/Insert a name for your pet/i)
    ).not.toBeInTheDocument()
    fireEvent.click(submitElement)
    expect(screen.getByText(/Insert a name for your pet/i)).toBeInTheDocument()
    expect(onSubmit).not.toHaveBeenCalled()
    fireEvent.change(screen.getByLabelText(/Name/i), {
      target: { value: name },
    })
    expect(
      screen.queryByText(/Insert a name for your pet/i)
    ).not.toBeInTheDocument()
    fireEvent.click(submitElement)
    expect(onSubmit).toHaveBeenCalled()
    expect(onSubmit.mock.calls[0][0]).toEqual({
      name,
      description: '',
      image: '',
    })
  })

  test('initialized with a given pet', () => {
    const garfield = { id: 1, name: 'Garfield', description: 'Cat' }
    render(<PetForm onSubmit={onSubmit} editingPet={garfield} />)
    const submitElement = screen.getByText(/Save pet/i)
    const removeElement = screen.queryByText(/Remove pet/i)
    expect(removeElement).not.toBeInTheDocument()
    fireEvent.click(submitElement)
    expect(onSubmit).toHaveBeenCalled()
    expect(onSubmit.mock.calls[0][0].name).toEqual('Garfield')
    fireEvent.change(screen.getByLabelText(/Name/i), {
      target: { value: 'Odie' },
    })
    fireEvent.click(submitElement)
    expect(onSubmit.mock.calls[1][0].name).toEqual('Odie')
  })

  test('allows to remove if function is added', () => {
    const mockRemove = jest.fn()
    render(<PetForm onSubmit={() => null} onRemove={mockRemove} />)
    const submitElement = screen.getByText(/Save pet/i)
    const removeElement = screen.getByText(/Remove pet/i)
    expect(submitElement).toBeInTheDocument()
    expect(removeElement).toBeInTheDocument()
    expect(mockRemove).not.toHaveBeenCalled()
    fireEvent.click(removeElement)
    expect(mockRemove).toHaveBeenCalled()
  })
})
