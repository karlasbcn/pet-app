/* eslint-disable testing-library/no-unnecessary-act */
/* eslint-disable testing-library/no-node-access */
/* eslint-disable testing-library/no-container */
import { render, screen, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { act } from 'react-dom/test-utils'
import ImagePicker from './ImagePicker'

const FAKE_DATA_URI = 'data:image/png;base64,'

describe('ImagePicker', () => {
  test('renders without image and triggers onChange with the file content', async () => {
    const mockOnChange = jest.fn()
    const mockFile = new File(['abcdef'], 'garfield.png', { type: 'image/png' })
    const { container } = render(
      <ImagePicker label="Picker" value="" onChange={mockOnChange} />
    )
    const input = container.querySelector(
      "input[type='file']"
    ) as HTMLInputElement
    expect(input).toBeTruthy()
    expect(screen.getByText('Picker')).toBeInTheDocument()
    expect(screen.queryByRole('img')).not.toBeInTheDocument()
    await act(async () => {
      await userEvent.upload(input, mockFile)
    })
    await waitFor(() => expect(mockOnChange).toHaveBeenCalled())
    expect(mockOnChange.mock.calls[0][0].includes(FAKE_DATA_URI)).toBe(true)
  })
  test('renders with image', () => {
    const { container } = render(
      <ImagePicker label="Picker" value={FAKE_DATA_URI} onChange={() => null} />
    )
    const image = container.querySelector('img') as HTMLImageElement
    expect(image).toBeTruthy()
    expect(image.getAttribute('src')).toEqual(FAKE_DATA_URI)
  })
})
