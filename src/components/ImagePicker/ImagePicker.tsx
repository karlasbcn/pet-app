import { Image, Box, Text } from '@mantine/core'
import { Dropzone, IMAGE_MIME_TYPE } from '@mantine/dropzone'
import { Camera } from 'components/icons'
import { dropZoneStyle, imageStyle } from './ImagePicker.styles'

const MAX_FILESIZE = 1024 * 1024
interface ImagePickerProps {
  value: string
  onChange: (value: string) => void
  label: string
}

function readFile([file]: File[]) {
  return new Promise<string>((resolve, reject) => {
    const reader = new FileReader()
    reader.onloadend = function () {
      resolve(reader.result as string)
    }
    reader.onerror = function () {
      reject()
    }
    reader.readAsDataURL(file)
  })
}

export default function ImagePicker({
  value,
  onChange,
  label,
}: ImagePickerProps) {
  const hasValue = Boolean(value)

  async function handleDrop(files: File[]) {
    try {
      const result = await readFile(files)
      if (result) {
        onChange(result)
      }
    } finally {
    }
  }

  return (
    <Dropzone
      onDrop={handleDrop}
      maxSize={MAX_FILESIZE}
      accept={IMAGE_MIME_TYPE}
      sx={dropZoneStyle(hasValue)}
    >
      {hasValue ? <Image sx={imageStyle} src={value} fit="cover" /> : null}
      <Box className="label">
        <Camera />
        <Text fw="bold" fz="20px">
          {label}
        </Text>
      </Box>
    </Dropzone>
  )
}
