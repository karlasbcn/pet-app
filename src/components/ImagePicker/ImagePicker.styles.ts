import type { MantineTheme } from '@mantine/core'

const HEIGHT = 310

export function dropZoneStyle(hasValue: boolean) {
  return function (theme: MantineTheme) {
    return {
      height: HEIGHT,
      padding: 0,
      marginBottom: theme.spacing.lg,
      borderRadius: theme.spacing.sm,
      borderStyle: hasValue ? 'solid' : 'dashed',
      '& .mantine-Dropzone-inner': {
        height: '100%',
      },
      '& .label': {
        ...theme.fn.cover(),
        margin: 'auto',
        width: 269,
        height: 64,
        background: theme.fn.rgba(theme.colors.gray[7], hasValue ? 0.35 : 1),
        transition: 'background 0.3s ease',
        padding: theme.spacing.lg,
        borderRadius: theme.spacing.xl,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        '& svg': {
          marginRight: theme.spacing.sm,
        },
        '& .mantine-Text-root': {
          color: 'white',
        },
      },
      '&:hover .label': {
        background: theme.colors.gray[7],
      },
    }
  }
}

export function imageStyle(theme: MantineTheme) {
  return {
    height: '100%',
    '& img': {
      borderRadius: theme.spacing.sm,
      height: `${HEIGHT - 4}px !important`,
    },
  }
}
