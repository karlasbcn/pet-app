import { Title as MantineTitle, Flex } from '@mantine/core'

interface TitleProps {
  text: string
  icon?: JSX.Element
}

export default function Title({ text, icon }: TitleProps) {
  const title = (
    <MantineTitle
      fz={icon ? 'lg' : 'xl'}
      fw="bold"
      ff="Averta"
      pb={icon ? 0 : 16}
      color="gray.9"
    >
      {text}
    </MantineTitle>
  )
  if (!icon) {
    return title
  }
  return (
    <Flex align="center" pb={16}>
      <Flex align="center" justify="center" mr={8}>
        {icon}
      </Flex>
      {title}
    </Flex>
  )
}
