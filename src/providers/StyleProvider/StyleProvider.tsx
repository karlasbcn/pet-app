import { MantineProvider, Global } from '@mantine/core'
import { theme, fontFaces } from './theme'

export default function StyleProvider(props: { children: JSX.Element }) {
  return (
    <MantineProvider theme={theme} withGlobalStyles withNormalizeCSS>
      <Global styles={fontFaces} />
      {props.children}
    </MantineProvider>
  )
}
