import { MantineTheme, DEFAULT_THEME } from '@mantine/core'

const colors: MantineTheme['colors'] = {
  ...DEFAULT_THEME.colors,
  gray: [
    '#F4F5F6',
    '#E6E8EB',
    '#DDDDDD',
    '#CFD7DE',
    '#B8BBC1',
    '#A5A9AE',
    '#85888E',
    '#62686E',
    '#404347',
    '#1F2124',
  ],
  green: [
    '#dffef2',
    '#b8f5de',
    '#8fedca',
    '#66e6b5',
    '#3edf9f',
    '#178C5F',
    '#207E4F',
    '#0e6e4a',
    '#02432c',
    '#00180c',
  ],
  blue: [
    '#e4f1ff',
    '#bdd1f6',
    '#95b3eb',
    '#6c95e0',
    '#295dbc',
    '#2E67D1',
    '#1e4893',
    '#13346b',
    '#081f43',
    '#000a1c',
  ],
}

const fontSizes: MantineTheme['fontSizes'] = {
  xs: 8,
  sm: 10,
  md: 13,
  lg: 26,
  xl: 32,
}

const spacing: MantineTheme['spacing'] = {
  ...DEFAULT_THEME.spacing,
  lg: 20,
  xl: 32,
}

export const theme: MantineTheme = {
  ...DEFAULT_THEME,
  colors,
  fontSizes,
  spacing,
  fontFamily: 'Averta',
  primaryColor: 'green',
  components: {
    Text: {
      styles: (theme) => ({
        root: {
          color: theme.colors.gray[9],
        },
      }),
    },
  },
}

export const fontFaces = [
  {
    '@font-face': {
      fontFamily: 'Averta',
      src: 'url("./Averta-Regular.woff") format("truetype")',
      fontStyle: 'normal',
    },
  },
]
