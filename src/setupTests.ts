import '@testing-library/jest-dom'

jest.mock('react-router-dom', () => ({
  ...(jest.requireActual('react-router-dom') as any),
  useNavigate: () => jest.fn(),
  useParams: jest.fn(),
  Navigate: () => 'Navigate component',
}))
